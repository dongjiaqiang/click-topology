package storm.book.bolt;

import static storm.book.util.Conf.REDIS_HOST_KEY;
import static storm.book.util.Conf.REDIS_PORT_KEY;
import static storm.book.util.Conf.IP;
import static storm.book.util.Conf.CLIENT_KEY;
import static storm.book.util.Conf.URL;
import static storm.book.util.Conf.UNIQUE;
import static storm.book.util.Conf.REDIS_TIMEOUT_KEY;

import static storm.book.util.Conf.connectToRedis;

import java.util.Map;

import redis.clients.jedis.Jedis;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;


//处理重复访客信息Bolt

public class RepeatVisitBolt extends BaseRichBolt{

	private OutputCollector collector;
	
	private Jedis jedis;
	private String host;
	private int port;
	private int timeOut;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context,
			OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector=collector;
		host=stormConf.get(REDIS_HOST_KEY).toString();
		port=Integer.parseInt(stormConf.get(REDIS_PORT_KEY).toString());
		timeOut=Integer.parseInt(stormConf.get(REDIS_TIMEOUT_KEY).toString());
		this.jedis=connectToRedis(host, port,timeOut);
		
	}

	//这个方法每收到一个元组调用一次.对于每个收到的元组,bolt会发射一些元组
	
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unused")
		String ip=input.getStringByField(IP);
		String clientKey=input.getStringByField(CLIENT_KEY);
		String url=input.getStringByField(URL);
		
		
		String key=url+":"+clientKey;
		
		String value=jedis.get(key);
		
		if(value==null||value.equals("nil")){
			jedis.set(key, "visited");
			collector.emit(new Values(clientKey,url,Boolean.TRUE.toString()));			
		}else{
			collector.emit(new Values(clientKey,url,Boolean.FALSE.toString()));
		}
		
	/*	//确认元组处理成功
		collector.ack(input);
		
		//确认元组处理失败
		collector.fail(input);*/
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(new Fields(CLIENT_KEY,URL,UNIQUE));
	}

}
