package storm.book.bolt;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import redis.clients.jedis.Jedis;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;

import static storm.book.util.Conf.CITY;
import static storm.book.util.Conf.COUNTRY;
import static storm.book.util.Conf.REDIS_HOST_KEY;
import static storm.book.util.Conf.REDIS_PORT_KEY;
import static storm.book.util.Conf.REDIS_TIMEOUT_KEY;
import static storm.book.util.Conf.connectToRedis;

class CountryStats{
	
	private int countryTotal=0;
	private static final int COUNT_INDEX=0;
	private static final int PERCENTAGE_INDEX=1;
	private String countryName;
	
	public CountryStats(String countryName) {
		super();
		this.countryName = countryName;
	}
	
	private Map<String, List<Integer>> cityStats=new HashMap<String, List<Integer>>();
	
	public CountryStats cityFound(String cityName){
		countryTotal++;
		if(cityStats.containsKey(cityName)){
			cityStats.get(cityName).set(COUNT_INDEX, cityStats.get(cityName).get(COUNT_INDEX).intValue()+1);			
		}else{
			List<Integer> list=new LinkedList<Integer>();
			
			list.add(1);
			list.add(0);
			cityStats.put(cityName, list);
		}
		 double percent=cityStats.get(cityName).get(COUNT_INDEX)/(double)countryTotal;
		 cityStats.get(cityName).set(PERCENTAGE_INDEX, (int)percent);
		 return this;
	}
	   
	public int getCountryTotal(){ return countryTotal;}
	public int getCityTotal(String cityName){
		return cityStats.get(cityName).get(COUNT_INDEX).intValue();
	}
	
	public void save(Jedis jedis){
		StringBuilder sb=new StringBuilder();
		sb.append(countryName+" : "+countryTotal+" city stats : ");
		for(Entry<String,List<Integer>> cityStat:cityStats.entrySet()){
			sb.append(cityStat.getKey()+" "+cityStat.getValue().get(0)+" "+cityStat.getValue().get(1)+" ");
		}
		jedis.set(countryName,sb.toString());
	}
    
}

//地理位置信息统计Bolt
public class GeoStatsBolt extends BaseRichBolt{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private OutputCollector collector;
	
	private Jedis jedis;
	private String host;
	private int port;
	private int timeOut;
	private Map<String, CountryStats> countryStatsMap=new HashMap<String, CountryStats>();
	
	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context,
			OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector=collector;
		host=stormConf.get(REDIS_HOST_KEY).toString();
		port=Integer.parseInt(stormConf.get(REDIS_PORT_KEY).toString());
		timeOut=Integer.parseInt(stormConf.get(REDIS_TIMEOUT_KEY).toString());
		jedis=connectToRedis(host, port, timeOut);
	}

	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		String country=input.getStringByField(COUNTRY);
		String city=input.getStringByField(CITY);
		if(countryStatsMap.containsKey(country))
			countryStatsMap.get(country).cityFound(city).save(jedis);
		else{
			CountryStats stats=new CountryStats(country);
			stats.cityFound(city).save(jedis);
			countryStatsMap.put(country, stats);
		}
		
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		
	}

}
