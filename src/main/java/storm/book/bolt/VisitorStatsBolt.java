package storm.book.bolt;

import java.util.Map;

import redis.clients.jedis.Jedis;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import static storm.book.util.Conf.REDIS_HOST_KEY;
import static storm.book.util.Conf.REDIS_PORT_KEY;
import static storm.book.util.Conf.REDIS_TIMEOUT_KEY;
import static storm.book.util.Conf.UNIQUE;
import static storm.book.util.Conf.connectToRedis;


//访问统计Bolt
public class VisitorStatsBolt extends BaseRichBolt{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private OutputCollector collector;
	private Jedis jedis;
	private String host;
	private int timeOut;
	private int port;
	
	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context,
			OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector=collector;
		
		host=stormConf.get(REDIS_HOST_KEY).toString();
		port=Integer.parseInt(stormConf.get(REDIS_PORT_KEY).toString());
		timeOut=Integer.parseInt(stormConf.get(REDIS_TIMEOUT_KEY).toString());
		jedis=connectToRedis(host, port,timeOut);
	}

	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		boolean unique=Boolean.parseBoolean(input.getStringByField(UNIQUE));
		String result=this.jedis.get("total");
		if(result==null||result.equals("nil"))
			this.jedis.set("total","1");
		else{
			int total=Integer.parseInt(result);
			this.jedis.set("total",String.valueOf(++total));
		}
		
		if(unique){
			result=this.jedis.get("uniqueCount");
			if(result==null||result.equals("nil"))
				this.jedis.set("uniqueCount","1");
			else{
				int uniqueCount=Integer.parseInt(result);
				this.jedis.set("uniqueCount", String.valueOf(++uniqueCount));
			}
		}
			
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		
	}

}
