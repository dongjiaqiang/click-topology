package storm.book.bolt;

import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import static storm.book.util.Conf.ipToCityMap;
import static storm.book.util.Conf.ipToCountryMap;
import static storm.book.util.Conf.IP;
import static storm.book.util.Conf.CITY;
import static storm.book.util.Conf.COUNTRY;

//处理地理位置信息的Bolt

public class GeographyBolt extends BaseRichBolt{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private OutputCollector collector;
	public void prepare(@SuppressWarnings("rawtypes") Map stormConf, TopologyContext context,
			OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector=collector;
	}

	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		String ip=input.getStringByField(IP);
		String city=ipToCityMap.get(ip);
		String country=ipToCountryMap.get(city);
		this.collector.emit(new Values(country,city));
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(new Fields(COUNTRY,CITY));
	}

}
