package storm.book.spout;

import java.util.Map;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;



import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import static storm.book.util.Conf.REDIS_HOST_KEY;
import static storm.book.util.Conf.REDIS_PORT_KEY;
import static storm.book.util.Conf.REDIS_TIMEOUT_KEY;
import static storm.book.util.Conf.IP;
import static storm.book.util.Conf.CLIENT_KEY;
import static storm.book.util.Conf.URL;
import static storm.book.util.Conf.connectToRedis;

//Storm实时处理系统

//Storm是一套分布式的,可靠的,可容错的用于处理流式数据的系统.

//处理工作会被委派给不同类型的组件,每个组件负责一项简单的,特定的处理任务.

//Storm集群的输入流由名为spout的组件负责.spout将数据传递给名为bolt的组件,后者以某种方式处理这些数据.
//bolt以某种存储方式持久化这些数据,或将它们传递给另外的bolt.

//Storm集群就是有一系列bolt组件组成的链条,每个bolt对spout曝露出来的数据做某种方式的处理

//Storm组件

//Spout组件---负责发射数据(数据源)---继承BaseRichSpout抽象类

public class ClickSpout extends BaseRichSpout{

	@SuppressWarnings("unused")
	private static Logger LOG=Logger.getLogger(ClickSpout.class);
	private Jedis jedis;
	private String host;
	private int port;
	private int timeOut;
	private SpoutOutputCollector collector;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//这是任何spout都调用的第一个方法---open方法
	
	//1.Map conf---在定义Topology时候被创建
	//2.TopologyContext context---包含所有Topology数据
	//3.SpoutOutputCollector collector---使得我们可以发射将被bolt处理的数据
	
	public void open(@SuppressWarnings("rawtypes") Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		// TODO Auto-generated method stub
		host=conf.get(REDIS_HOST_KEY).toString();
		port=Integer.parseInt(conf.get(REDIS_PORT_KEY).toString());
		timeOut=Integer.parseInt(conf.get(REDIS_TIMEOUT_KEY).toString());
		this.collector=collector;		
		this.jedis=connectToRedis(host,port,timeOut);
	}

	//这个方法中我们发射将被bolt处理的值
	public void nextTuple() {
		// TODO Auto-generated method stub
		String content=jedis.rpop("count");
		if(content==null||"nil".equals(content)){
			try{
				Thread.sleep(100);
			}catch (InterruptedException e) {
				// TODO: handle exception
			}
		}else{
			String[] fields=content.split(",");
			//这里发射要bolt处理的值
			collector.emit(new Values(fields[0],fields[1],fields[2]));
		}
	}

	//这个方法定义了输出字段的情况,这里定义了三个字段
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(new Fields(IP,URL,CLIENT_KEY));
	}

}
