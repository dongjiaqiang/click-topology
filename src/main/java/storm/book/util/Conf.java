package storm.book.util;

import java.util.HashMap;
import java.util.Map;

import redis.clients.jedis.Jedis;

public class Conf {
	private Conf(){
		
	}
	

	public static final String REDIS_HOST_KEY="redis.host.key";
	public static final String REDIS_PORT_KEY="redis.port.key";
	public static final String REDIS_TIMEOUT_KEY="redis.timeout.key";
	
	public static final String IP="ip";
	public static final String URL="url";
	public static final String CLIENT_KEY="client.key";
	public static final String UNIQUE="visited";
	public static final String COUNTRY="country";
	public static final String CITY="city";
	
	public static final String DEFAULT_JEDIS_PORT="6379";
	public static final String DEFAULT_JEDIS_HOST="192.168.163.133";
	public static final String DEFAULT_JEDIS_TIMEOUT="5000";
	
	
	public static final Map<String, String> ipToCityMap =new HashMap<String, String>();
	public static final Map<String, String> ipToCountryMap=new HashMap<String, String>();
	static{
		ipToCityMap.put("192.168.163.1", "Shanghai");
		ipToCityMap.put("192.168.163.8", "Shanghai");
		ipToCityMap.put("192.168.163.9", "Shanghai");
		
		ipToCityMap.put("192.168.163.2", "Tianjin");
		ipToCityMap.put("192.168.163.10", "Tianjin");
		ipToCityMap.put("192.168.163.11", "Tianjin");
		ipToCityMap.put("192.168.163.12", "Tianjin");
		ipToCityMap.put("192.168.163.13", "Tianjin");
		
		ipToCityMap.put("192.168.163.3", "Seoul");
		ipToCityMap.put("192.168.163.14", "Seoul");
		
		ipToCityMap.put("192.168.163.4", "Chicago");
		ipToCityMap.put("192.168.163.15", "Chicago");
		ipToCityMap.put("192.168.163.16", "San Francisco");
		ipToCityMap.put("192.168.163.17", "San Francisco");
		
		ipToCityMap.put("192.168.163.5", "Taipei");
		
		ipToCityMap.put("192.168.163.6", "HongKong");
		
		ipToCityMap.put("192.168.163.7", "London");
		
		ipToCityMap.put("192.168.163.15", "Beijing");
	
		
		ipToCountryMap.put("Beijing", "China");
		ipToCountryMap.put("Shanghai", "China");
		ipToCountryMap.put("Tianjin", "China");
		ipToCountryMap.put("Taipei", "China");
		ipToCountryMap.put("HongKong", "China");
		
		ipToCountryMap.put("Seoul", "Korea");
		ipToCountryMap.put("Chicago", "USA");
		ipToCountryMap.put("San Francisco", "USA");
		ipToCountryMap.put("London", "UK");
	}
			
	public static Jedis connectToRedis(String host,int port,int timeOut){		
		
		Jedis jedis=new Jedis(host, port,timeOut);
		
		
		jedis.connect();
		
		return jedis;
		
	}
	
	public static Jedis connectToRedis(String host,int port){
		Jedis jedis=new Jedis(host, port);
		jedis.connect();
		return jedis;
	}

}
