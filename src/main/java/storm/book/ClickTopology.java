package storm.book;



import storm.book.bolt.GeoStatsBolt;
import storm.book.bolt.GeographyBolt;
import storm.book.bolt.RepeatVisitBolt;
import storm.book.bolt.VisitorStatsBolt;
import storm.book.spout.ClickSpout;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.utils.Utils;
import static storm.book.util.Conf.COUNTRY;
import static storm.book.util.Conf.REDIS_HOST_KEY;
import static storm.book.util.Conf.REDIS_PORT_KEY;
import static storm.book.util.Conf.REDIS_TIMEOUT_KEY;
import static storm.book.util.Conf.DEFAULT_JEDIS_HOST;
import static storm.book.util.Conf.DEFAULT_JEDIS_PORT;
import static storm.book.util.Conf.DEFAULT_JEDIS_TIMEOUT;


//Storm是一套分布式的,可靠的,可容错的用于处理流式数据的系统.处理工作会分别分派给不同类型的组件,每个组件负责一项简单的,特定的处理任务.
//Storm集群的输入流由名为spout组件负责.Spout将数据传递给名为bolt的组件,后者将以某种方式处理这些数据.


//Topologies---怎么在一个storm topology的不同组件间传递元组,怎么在一个运行的storm集群上部署topology

//流分组---指定每个bolt消费哪些流和这些流被怎么消费

//在设计一个topology时,需要定义数据在组件间怎么交换(流是怎么被bolt消费的)


public class ClickTopology {

	/**
	 * @param args
	 * @throws InvalidTopologyException 
	 * @throws AlreadyAliveException 
	 */
	public static void main(String[] args) throws AlreadyAliveException, InvalidTopologyException {
		// TODO Auto-generated method stub
		TopologyBuilder builder=new TopologyBuilder();
		
		
		Config conf=new Config();
		conf.put(REDIS_PORT_KEY,DEFAULT_JEDIS_PORT);
		conf.put(REDIS_HOST_KEY, DEFAULT_JEDIS_HOST);
		conf.put(REDIS_TIMEOUT_KEY, DEFAULT_JEDIS_TIMEOUT);
		
		builder.setSpout("clickSpout", new ClickSpout(),1);
		
		
		//流分组策略
		
		//Shuffle分组---这是最常用的分组发送---使用一个参数(源组件),源组件会发射元组到一个随机选择的bolt并确保每个消费者会收到等数量的元组
		
		//Fields分组---允许你基于元组的一个或多个域来控制元组怎么被发送到bolt.确保一个联合域中给定的值集合总是会被送到相同的bolt
		
		//all分组---发送每个元组的一份单独拷贝到所有目标task
		
		//global分组---所有元组被发送到id最小的task
		
		//
		
		
		//Bolt的第一层实现
		builder.setBolt("repeatBolt", new RepeatVisitBolt(),2).shuffleGrouping("clickSpout");
		
		builder.setBolt("geographyBolt", new GeographyBolt(),2).shuffleGrouping("clickSpout");
		
		//Bolt的第二层实现
		builder.setBolt("totalStats", new VisitorStatsBolt(),1).globalGrouping("repeatBolt");
		
		builder.setBolt("geoStats", new GeoStatsBolt(),2).fieldsGrouping("geographyBolt", new Fields(COUNTRY));
		
		if(args.length==1){
			conf.setDebug(true);
			LocalCluster cluster=new LocalCluster();
			cluster.submitTopology("test", conf, builder.createTopology());
			int runtTime=Integer.parseInt(args[0]);
			if(runtTime>0){
				Utils.sleep(runtTime);
			}
			if(cluster!=null){
				cluster.killTopology("test");
				cluster.shutdown();
			}
		}else{
			conf.setNumWorkers(4);
			StormSubmitter.submitTopology("ClickTopology", conf, builder.createTopology());
		}
		
		
	}

}
