package storm.book;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import redis.clients.jedis.Jedis;
import static storm.book.util.Conf.DEFAULT_JEDIS_HOST;
import static storm.book.util.Conf.DEFAULT_JEDIS_PORT;
import static storm.book.util.Conf.DEFAULT_JEDIS_TIMEOUT;

@RunWith(value=Parameterized.class)
public class TestRepeatVisitBolt extends StormTestCase{

	//自定义一些参数
	@Parameterized.Parameters
	public static Collection<Object[]> data(){
		Object[][]data=new Object[][]{
				{"192.168.33.100","Client1","myintranet.com","false"},
				{"192.168.33.100","Client1","myintranet.com","false"},
				{"192.168.33.101","Client1","myintranet.com","true"},
				{"192.168.33.102","Client1","myintranet.com","false"}				
		};
		return Arrays.asList(data);
	}
	
	//使用Redis进行测试前,必须进行初始化一些基本配置信息
	@BeforeClass
	public static void setupJedis(){
		String host=DEFAULT_JEDIS_HOST;
		int port=Integer.valueOf(DEFAULT_JEDIS_PORT);
		int timeout=Integer.valueOf(DEFAULT_JEDIS_TIMEOUT);
		Jedis jedis=new Jedis(host, port, timeout);
		jedis.flushDB();
        Iterator<Object[]> it=data().iterator();
        while(it.hasNext()){
        	Object[] values=it.next();
        	if(values[3].equals("false")){
        		String key=values[2]+":"+values[1];
        		jedis.set(key, "visited");//unique, meaning it must exist
        	}
        }
	}
	
	public void textExecute(){
		
	}
}
